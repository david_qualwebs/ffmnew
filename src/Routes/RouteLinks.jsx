import React from 'react';
import {Switch,Route,Redirect} from "react-router-dom";
import HomePage from "../Components/Static/HomePage";
//import CartSideBar from "../Components/Static/CartSideBar";

const RouteLinks = () => {
    return (
        <React.Fragment>
            <Switch>
             <Route exact path="/" component={HomePage}/>
             {/*<Route exact path="/cartsidebar" component={CartSideBar}/>*/}
             <Redirect to="/"/>
            </Switch>
        </React.Fragment>
    );
};

export default RouteLinks;
