import React from 'react';
import Header from "../Header/Header";
import Header1 from "../Header/Header1";
import Footer from "../Footer/Footer";

const Layout = (props) => {
    return (
        <React.Fragment>
          <Header/>
         
          <div id="wrapper">
              <Header1/>
              <div class="content-padding" id="page-content-wrapper">
               {props.children}
          
              <Footer/>
              </div>
          </div> 
        </React.Fragment>
    );
};

export default Layout;
