import React,{useContext} from 'react';
import LogoImg from "../../assets/images/logo2.png";
import HomeImg from "../../assets/images/home.png";
import CartSideBar from "./CartSideBar";
import {Context} from "../../GlobalState/GlobalState";

const Header1 = () => {
   
    const [state,setState] = useContext(Context);
    /*if(state===1)
    {console.log(state);}
    else{console.log(state+1);}*/
    console.log(state);
    return (
        <React.Fragment>
            <header id="header" className="d-md-none d-none d-lg-none d-xl-block">
                <div className="bg-white header">
                    <div className="container-fluid container-width">
                        <div className="row">
                            <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                                <div className="mid-head-details">
                                    <div className="logo-col">
                                        <img src={LogoImg} alt="Not Found" className="img-fluid" />
                                    </div>
                                    <div className="flex-col">
                                        <div className="head-content">
                                            <div className="search-bar-col">
                                                <div className="search-bar">
                                                    <form className="input-search">
                                                        <input className="form-control" type="search" placeholder="Search" id="example-search-input" />
                                                        <i className="fa fa-search"></i>
                                                    </form>
                                                </div>
                                            </div>

                                            <div className="drop-col">
                                                <div className="pickup-selection form-group">
                                                    <div className="dropdown">
                                                        <button className="btn dropdown-toggle" type="button" data-toggle="modal"
                                                            data-target="#exampleModal" aria-haspopup="true" aria-expanded="false">
                                                            <img src={HomeImg} alt="Not Found" /><span>Pickup from - </span><span className="pickup"> Farmer's Fresh
                                                              Meat on Cullen</span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="menu-col top-header">
                                                <ul className="list-group list-group-horizontal content-right">
                                                    <li>
                                                        <a href="#"><i className="fas fa-exchange-alt"></i> Reorder</a>
                                                    </li>
                                                    <li>
                                                        <a href="#"><i className="fa fa-list" aria-hidden="true"></i> Lists</a>
                                                    </li>
                                                    <li>
                                                        <a href="#"><i className="fa fa-user"></i> Hi, Abdallah</a>
                                                    </li>
                                                    <li id="menu-toggle">
                                                        <a href="#"><i className="fa fa-shopping-cart" aria-hidden="true"></i> My Cart</a>
                                                        <span className="badge badge-light ">{state}</span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <CartSideBar/>
        </React.Fragment>
    );
};

export default Header1;
