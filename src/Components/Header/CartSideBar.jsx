import React from 'react';
import Img6 from "../../assets/images/new-img6.png";
import Img7 from "../../assets/images/new-img7.png";
import Img5 from "../../assets/images/new-img5.png";

const CartSideBar = () => {

    return (
        <React.Fragment>
        <div id="sidebar-wrapper">
        <div className="container1">
  
          <div className="background">
            <div className="closeburger"></div>
          </div>
  
          <div className="sidebar-front">
            <div className="row">
              <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                <div className="food-images"><img src={Img6} alt="Not Found"/></div>
                <div className="food-images"><img src={Img7} alt="Not Found"/></div>
                <div className="food-images"><img src={Img6} alt="Not Found"/></div>
                <div className="food-images"><img src={Img7} alt="Not Found"/></div>
              </div>
            </div>
          </div>
  
          <div className="main-cart">
            <div className="cart-padding">
              <div className="cart-title">
                <h6>Cart</h6>
              </div>
              <div className="row">
                <div className="col-12 col-md-12 col-lg-12 col-xl-12">
                  <div className="estimate-order">
                    <h6>$379<span>.56</span></h6>
                    <p>Estimated order value</p>
                  </div>
                  <div className="proceed-button">
                    <button className="btn btn-proceed">Proceed to checkout</button>
                  </div>
                </div>
              </div>
            </div>
  
  
            <div className="row">
              <div className="col-xl-12">
                <div className="cart-food-details">
                  <h6>Farmer’s fresh Meat <span>3 item(s)</span></h6> 
                </div>
                <div className="cart-padding">
                 {/* <!-------------food1------------------------->*/}
                  <div className="food-details">
                    <div><img src={Img5} className="img-fluid" alt="Not Found"/></div>
                    <div className="cart-inner-details">
                      <h6>USDA Primen Angus Boneless New York Stripe</h6>
                      <div className="row">
                        <div className="col-12 col-md-8 col-xl-7">
                          <div className="food-details mt-2">
                            <div className="qty-button">
                              <div className="num-block skin-2">
                                <div className="num-in">
                                  <span className="minus dis"></span>
                                  <input type="text" className="in-num" value="1" readonly=""/>
                                  <span className="plus"></span>
                                </div>
                              </div>
                            </div>
                            <div className="cart-price product-details p-0">
                              <div className="food-content">
                                <p className="price"><sup>$</sup><span>13</span><sup>33</sup></p>
                              </div>
                            </div>
                          </div>
                        </div>
  
                        <div className="col-12 col-md-8 col-xl-2 offset-xl-2 close-btn">
                          <h6>x</h6>
                        </div>
                      </div>
                    </div>
                  </div>
                  {/*<!-------------/food1------------------------->*/}
  
                  <hr/>
  
                  {/*<!-------------food1------------------------->*/}
                  <div className="food-details">
                    <div><img src={Img5} className="img-fluid" alt="Not Found"/></div>
                    <div className="cart-inner-details">
                      <h6>USDA Primen Angus Boneless New York Stripe</h6>
                      <div className="row">
                        <div className="col-12 col-md-8 col-xl-7">
                          <div className="food-details mt-2">
                            <div className="qty-button">
                              <div className="num-block skin-2">
                                <div className="num-in">
                                  <span className="minus dis"></span>
                                  <input type="text" className="in-num" value="1" readonly=""/>
                                  <span className="plus"></span>
                                </div>
                              </div>
                            </div>
                            <div className="cart-price product-details p-0">
                              <div className="food-content">
                                <p className="price"><sup>$</sup><span>13</span><sup>33</sup></p>
                              </div>
                            </div>
                          </div>
                        </div>
  
                        <div className="col-12 col-md-8 col-xl-2 offset-xl-2 close-btn">
                          <h6>x</h6>
                        </div>
                      </div>
                    </div>
                  </div>
                  {/*<!-------------/food1------------------------->*/}
  
                  <hr/>
  
                  {/*<!-------------food1------------------------->*/}
                  <div className="food-details">
                    <div><img src={Img5} className="img-fluid" alt="Not Found"/></div>
                    <div className="cart-inner-details">
                      <h6>USDA Primen Angus Boneless New York Stripe</h6>
                      <div className="row">
                        <div className="col-12 col-md-8 col-xl-7">
                          <div className="food-details mt-2">
                            <div className="qty-button">
                              <div className="num-block skin-2">
                                <div className="num-in">
                                  <span className="minus dis"></span>
                                  <input type="text" className="in-num" value="1" readonly=""/>
                                  <span className="plus"></span>
                                </div>
                              </div>
                            </div>
                            <div className="cart-price product-details p-0">
                              <div className="food-content">
                                <p className="price"><sup>$</sup><span>13</span><sup>33</sup></p>
                              </div>
                            </div>
                          </div>
                        </div>
  
                        <div className="col-12 col-md-8 col-xl-2 offset-xl-2 close-btn">
                          <h6>x</h6>
                        </div>
                      </div>
                    </div>
                  </div>
                  {/*<!-------------/food1------------------------->*/}
                </div>
              </div>
            </div>
          </div>
  
        </div>
  
        {/*<!-------------------container1------------------------>*/}
      </div>      
        </React.Fragment>
    );
};

export default CartSideBar;
