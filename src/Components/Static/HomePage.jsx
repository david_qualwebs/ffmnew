import React from 'react';
import SideBar from "../Common/SideBar";
import Banner from "../Common/Banner";
import FirstSection from "../HomePageComponents/FirstSection";
import Featured from "../HomePageComponents/Featured";
import MostPopular from "../HomePageComponents/MostPopular";
import AbsolutelySection from "../HomePageComponents/AbsolutelySection";


const HomePage = () => {
    return (
        <React.Fragment>
           <div className="container-fluid">
             <div className="row">
                <SideBar/> 
                <div class="col-12 col-md-10 col-lg-10 col-xl-9 column">
                    <Banner/>
                    <FirstSection/>
                    <Featured/>
                    <MostPopular/>
                    <AbsolutelySection/>
                    
                </div>
             </div>
           </div>
        </React.Fragment>
    );
};

export default HomePage;
