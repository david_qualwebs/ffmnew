import React from 'react';
import Card from '../Common/Card';
import {CardData} from "../HomePageComponents/Data";

const AbsolutelySection = () => {
    return (
        <React.Fragment>
            <div class="row">
            {
                CardData.map((val,index)=>{
                 return(
                     <Card
                       key={index}
                       image={val.image}
                     />
                 )
               })
             }
            </div>
        </React.Fragment>
    );
};

export default AbsolutelySection;
