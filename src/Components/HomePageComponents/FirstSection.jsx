import React from 'react';
import FoodCard from "../Common/FoodCard";
import Data from "../HomePageComponents/Data";

const FirstSection = (props) => {
    return (
        <React.Fragment>
        <div className="row display-details">
        
        {Data.map((val,index)=>{
            return(
               <FoodCard
                 key={index}
                 image={val.image}
               />
            )
        })}  

      </div>
      {/*<!-----------------------------/second section--------------------------->*/}
        </React.Fragment>
    );
};

export default FirstSection;
