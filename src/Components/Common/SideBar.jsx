import React from 'react';

const SideBar = () => {
    return (
        <React.Fragment>
            <div className="col-12 col-md-2 col-lg-2 col-xl-3 column1">
                <div className="sidebar d-md-none d-lg-block d-xl-block bg-white">
                    <h5>Category</h5>
                    <ul>
                        <li>
                            <a href="#">Beef <span>12</span></a>
                        </li>
                        <li>
                            <a href="#">Chicken <span>12</span></a>
                        </li>
                        <li>
                            <a href="#">Pork <span>12</span></a>
                        </li>
                        <li>
                            <a href="#">Turkey <span>12</span></a>
                        </li>
                        <li>
                            <a href="#">Lamb & Goat<span>12</span></a>
                        </li>

                        <li>
                            <a href="#">Deli <span>12</span></a>
                        </li>

                        <li>
                            <a href="#">Homemade Sausage <span>12</span></a>
                        </li>

                        <li>
                            <a href="#">Pantry <span>12</span></a>
                        </li>
                        <li>
                            <a href="#">Fruits & Vegetables <span>12</span></a>
                        </li>
                        <li>
                            <a href="#">Wholesale & Bulk <span>12</span></a>
                        </li>

                    </ul>
                </div>
            </div>
        </React.Fragment>
    );
};

export default SideBar;
