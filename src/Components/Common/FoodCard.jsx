import React,{useState,useContext} from 'react'
import Img5 from "../../assets/images/img5.png";
import Img7 from "../../assets/images/img7.png";
import Popup from 'reactjs-popup';
import 'reactjs-popup/dist/index.css';
import {Context} from "../../GlobalState/GlobalState";

const FoodCard = (props) => {
    
    const [state,setState] =useContext(Context);
    const [btnVal,setbtnVal] = useState(0);
    const [itemVal,setItemVal] = useState(1);

    const handleButton = () =>{
      setbtnVal(1);
      setState(itemVal);
    } 
    
    const decrement = () =>{
        if(itemVal===0){
           setbtnVal(0); 
        }
       
        else{
          setItemVal(itemVal-1);
          setState(itemVal);       
        }
    };
    
    const increment = () =>{
      setItemVal(itemVal+1);
      setState(itemVal);
    };


    return (
        <React.Fragment>
        <div className="food-column" data-aos="zoom-in" data-aos-easing="linear">
        <div className="product-details">
          <div className="onsale-details">
            <span>On sale</span>
          </div>
          <div className="top-img">
            <div> <img className="p" src={Img5} alt="Not Found"/></div>
            <div> <img className="b" src={Img7} alt="Not Found"/></div>
          </div>

          <div className="food-img">
            <img src={props.image} className="img-fluid" alt="Not Found"/>
          </div>

          <div className="food-content">
            <h6>USDA Primen Angus Boneless New York Stripe</h6>
            <p className="weight">Wt: 2.5lb <span>$3.49/lb</span></p>

            <div className="custom-checkbox">

              <div className="pickup-date payment-method">
                <input type="radio" id="myradio1" checked="checked" name="radio1"/>
                <label for="myradio1">
                  4lb
                </label>
              </div>

              <div className="pickup-date payment-method">
                <input type="radio" id="myradio2" name="radio1"/>
                <label for="myradio2">
                  4lb
                </label>
              </div>

              <div className="pickup-date payment-method">
                <input type="radio" id="myradio3" name="radio1"/>
                <label for="myradio3">
                  4lb
                </label>
              </div>



              <div className="dropdown more-size">
                <button className="btn  dropdown-toggle" type="button" id="dropdownMenuButton"
                  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i className="fa fa-ellipsis-h" aria-hidden="true"></i>
                </button>
                <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                  <div className="dropdown-item" href="#">
                    <div className="pickup-date payment-method">
                      <input type="radio" id="myradio1.1" checked="checked" name="radio1"/>
                      <label for="myradio1">
                        6lb
                      </label>
                    </div>
                  </div>
                  <div className="dropdown-item" href="#">
                    <div className="pickup-date payment-method">
                      <input type="radio" id="myradio2.1" name="radio1"/>
                      <label for="myradio2">
                        6lb
                      </label>
                    </div>
                  </div>
                </div>
              </div>
            </div>




            <p className="price"><sup>$</sup><span>13</span><sup>33</sup> <del>$14.33</del></p>

            <div className="buttons">
              <button className="cart-button" onClick={()=>handleButton()}>
                {btnVal===0? <span className="add-to-cart">Add to cart</span>:
                <div className="added">
                  <span style={{marginLeft:"-40%",fontSize:"25px",marginTop:"-2%"}}
                    onClick={()=>decrement()}>-</span>
                  <span>{itemVal}</span>
                  <span style={{marginLeft:"40%",fontSize:"20px"}}
                    onClick={()=>increment()}>+</span>  
                </div> 
                } 
              </button>
            </div>
            <div>
              <Popup trigger=
                {<a className="btn btn-list">Add to list</a>} 
                position="bottom center"
              >
              <div>Added to List !!</div>
              </Popup>
            </div>
          </div>
        </div>
      </div>
        </React.Fragment>
    );
};

export default FoodCard;
