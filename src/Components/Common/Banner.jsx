import React from 'react';

const Banner = () => {
    return (
        <React.Fragment>
            <div class="banner-details" data-aos="zoom-in" data-aos-easing="linear">
                <h5>Shop fresh at Fresh Meat & Grocery</h5>
                <p>Voted Houston’s Best Meat Market & Butcher Shop</p>
            </div>
        </React.Fragment>
    );
};

export default Banner;
