import React from 'react';

const Card = (props) => {
    return (
        <React.Fragment>
            <div class="col-12 col-md-4 col-lg-4 col-xl-4">
                <div class="fresh-food-details" data-aos="zoom-in" data-aos-easing="linear">
                    <div>
                        <img src={props.image} class="img-fluid" alt="Not Found" />
                    </div>
                    <h5>Absolutely Fresh Chicken</h5>
                    <p>100% Natural & Chemical Free</p>
                    <button class="btn btn-order">Order now</button>
                </div>
            </div>
        </React.Fragment>
    );
};

export default Card;
