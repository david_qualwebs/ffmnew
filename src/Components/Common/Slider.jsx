import React from 'react';
import Img5 from "../../assets/images/img5.png";
import Img7 from "../../assets/images/img7.png";

const Slider = (props) => {
    return (
        <React.Fragment>
            <div class="item">
                <div className="product-details">

                    <div className="top-img">
                        <div> <img className="p" src={Img5} alt="Not Found" /></div>
                        <div> <img className="b" src={Img7} alt="Not Found" /></div>
                    </div>

                    <div className="food-img">
                        <img src={props.image} className="img-fluid" alt="Not Found" />
                    </div>

                    <div className="food-content">
                        <h6>USDA Primen Angus Boneless New York Stripe</h6>
                        <p className="weight">Wt: 2.5lb <span>$3.49/lb</span></p>

                        <div className="custom-checkbox"></div>
                        <p className="price"><sup>$</sup><span>13</span><sup>33</sup> <del>$14.33</del></p>

                        <div className="buttons">
                            <button className="cart-button">
                                <span className="added">
                                    <div className="num-block skin-5">
                                        <div className="num-in">
                                            <span className="minus dis decrement " onclick="decrementQty()"> - </span>
                                            <input type="text" className="in-num" value="1" name="qty" />
                                            <span className="plus increment" onclick="incrementQty()">+</span>
                                        </div>
                                    </div>
                                </span>
                                <span className="add-to-cart">Add to cart</span> </button>
                        </div>
                        <div>
                            <a href="#" className="btn btn-list">Add to list</a>
                        </div>
                    </div>
                </div>

            </div>

        </React.Fragment>
    );
};

export default Slider;
