import React,{createContext,useState} from 'react'

const Context = createContext();

const GlobalState = (props) => {

    const [state,setState] =useState();

    return (
        <React.Fragment>
            <Context.Provider value={[state,setState]}>
              {props.children}
            </Context.Provider>
        </React.Fragment>
    );
};

export default GlobalState;
export {Context};
