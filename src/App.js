import React from "react";
import {BrowserRouter} from "react-router-dom";
import "./assets/css/home.css";
//import "./assets/css/home-media.css";
import Layout from "./Components/Layout/Layout";
import RouteLinks from "./Routes/RouteLinks";
import GlobalState from "./GlobalState/GlobalState";

function App() {
  
  return ( 
    <React.Fragment>
      <GlobalState>
        <BrowserRouter>
          <Layout> 
            <RouteLinks/>
          </Layout>
        </BrowserRouter>
      </GlobalState>
    </React.Fragment>
  );
};

export default App;
